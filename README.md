# Qualifying University of Texas Exams Study Materials (QUoTESM) #

### What is this repository for? ###

* Aerothermodynamics Qualifying Exam

### Contribution guidelines ###

* When provided practice exams/questions, please include solutions.
* Please add to & edit the LaTeX journal containing all the topics covered by the exam.

### Who do I talk to? ###

* msh81@utexas.edu